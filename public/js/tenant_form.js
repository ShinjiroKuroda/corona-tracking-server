
//非同期で住所取り込む
$(function () {
    //検索ボタンをクリックされたときに実行
    $("#search_btn").click(function () {
        //入力値をセット
        var param = { zipcode: $('#zipcode').val() }
        //zipcloudのAPIのURL
        var send_url = "http://zipcloud.ibsnet.co.jp/api/search";
        $.ajax({
            type: "GET",
            cache: false,
            data: param,
            url: send_url,
            dataType: "jsonp",
            success: function (res) {
                //結果によって処理を振り分ける
                if (res.status == 200) {
                    //処理が成功したとき
                    //該当する住所を表示
                    var html = '';
                    for (var i = 0; i < res.results.length; i++) {
                        var result = res.results[i];
                        $("#address_1").val(result.address1 + result.address2 + result.address3);
                        //エラー内容は消す
                        $('#zip_result').remove();
                    }
                } else {
                    //エラーだった時
                    //エラー内容を表示（必要であれば）
                    $('#zip_result').html(res.message);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    });
});

jQuery(document).on('change', '#tenantType', function(e) {

    var val = $('[name=tenant_category_name]').val();

    if (val.match('未選択')) {

      $("#capacity").attr('disabled',true);
      $('select#capacity option').remove();
      $option_1 = $('<option>').val("未選択").text("選択...").prop('selected', true);
      $("#capacity").append($option_1);
      $("#judleType").val("");
    } else if (val.match('飲食店|劇場・映画館|会議室|パチンコ店|キャバレー・バー・パブ|ライブハウス|マージャン店')) {
      $("#capacity").attr('disabled',false);
      $('select#capacity option').remove();
      $option_0 = $('<option>').val("未選択").text("選択...").prop('selected', true);
      $("#capacity").append($option_0);
      $("#judleType").val("席数");
      $option_1 = $('<option>').val("50席未満").text("50席未満");
      $option_2 = $('<option>').val("50〜300席未満").text("50〜300席未満");
      $option_3 = $('<option>').val("300席以上").text("300席以上");
      $("#capacity").append($option_1);
      $("#capacity").append($option_2);
      $("#capacity").append($option_3);

    } else {
      $("#capacity").attr('disabled',false);
      $('select#capacity option').remove();
      $option_0 = $('<option>').val("未選択").text("選択...").prop('selected', true);
      $("#capacity").append($option_0);
      $("#judleType").val("面積");
      $option_1 = $('<option>').val("1000m2以下").text("1000m2以下");
      $option_2 = $('<option>').val("1000m2以上").text("1000m2以上");
      $("#capacity").append($option_1);
      $("#capacity").append($option_2);

    }

});


//電話番号は全角入力できないようにする
jQuery(document).on('keydown', '#tel', function (e) {
    let k = e.keyCode;
    let str = String.fromCharCode(k);
    if (!(str.match(/[0-9]/) || (37 <= k && k <= 40) || k === 8 || k === 46)) {
        return false;
    }
});

jQuery(document).on('keyup', '#tel', function (e) {
    this.value = this.value.replace(/[^0-9]+/i, '');
});

jQuery(document).on('blur', '#tel', function () {
    this.value = this.value.replace(/[^0-9]+/i, '');
});

//同意しないとボタン反応しないようする
$(function () {
    $('#submit').attr('disabled', 'disabled');
    $('#check').click(function () {
        if ($(this).prop('checked') == false) {
            $('#submit').attr('disabled', 'disabled');
        } else {
            $('#submit').removeAttr('disabled');
        }
    });
});
