<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Tenant;
use App\Account;
use Mail;
use App\Mail\SendThank;

class UserController extends Controller
{
    //
    public function login($id) {
      try {
        //①事業者情報を登録
        $tenant_id = Crypt::decrypt($id);
        $tenant = \App\Tenant::find($tenant_id);
        if (!isset($tenant)) {
          abort('418','テナント情報が見つかりませんでした');
        }
        return view('user_login',compact('tenant','id'));
      } catch (\Exception $e){
        abort('418','エラーが発生しました');
      }
    }

    public function regist(Request $request) {
      try {
        //①事業者情報を登録
        $tenant_id = Crypt::decrypt($request->secret_code);
        $tenant = \App\Tenant::find($tenant_id);
        if (!isset($tenant)) {
          abort('418','登録に失敗しました');
        }

        $account = new Account();
        $account->email = $request->email;
        $account->tenant_id = $tenant_id;
        if (!$account->save()) {
          abort('418','登録に失敗しました');
        }

        Mail::to($request->email)->send(new SendThank($tenant));

        return redirect('/footprint/complete');
      } catch (\Exception $e){
        abort('418','エラーが発生しました');
      }
    }

}
