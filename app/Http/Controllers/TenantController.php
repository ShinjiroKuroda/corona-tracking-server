<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tenant;
use Mail;
use App\Mail\SendMail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Crypt;
use TCPDF;
use TCPDF_FONTS;
use App\Http\Requests\TenantPost;
use Illuminate\Support\Facades\Storage;

class TenantController extends Controller
{
    //店舗情報登録メソッド
    public function regist(TenantPost $request) {

      try {

        //① DBに事業者情報を登録
        $tenant = new Tenant();
        
        $request['location'] = $request['location_1'] . $request['location_2'];
        $request['password'] = encrypt($request['password']);

        //テナント保存処理
        if (!$tenant->fill($request->all())->save()) {
            abort('418','登録に失敗しました。');
        }
        // QRコードファイル名
        $qr_png_name = $tenant->id.'_qr.png';
        // QRコードを一旦images配下に作成(後で消す処理必須)
        QrCode::format('png')->size(150)->generate(url('/login') . '/' .Crypt::encrypt($tenant->id), public_path('/images/'.$qr_png_name));

        // PDF 生成メインA4 縦に設定
        $pdf = new TCPDF("P", "mm", "A4", true, "UTF-8" );
        // // ページ追加
        $pdf->addPage();
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        //日本語化対応フォント指定
        $pdf->SetFont('ipag', 'B', 12);

        // HTMLを描画、viewの指定と変数代入 - pdf_test.blade.php
        $pdf->writeHTML(view("qr_pdf", ['qr_png_name' => $qr_png_name])->render());
        //PDFを一旦images配下に作成
        $pdf->output(public_path('images').'/'.$tenant->id.'.pdf', 'F');

        //② メールアドレス宛にQRコードを送信する
        if(Mail::to($request->email)->send(new SendMail($tenant))) {
          //QRコード単体と印刷用QRコードpdfを削除
          \File::delete('images/'.$qr_png_name);
          \File::delete('images/'.$tenant->id.'.pdf');

          //④ 登録完了画面
          return redirect('/tenant/complete');
        } else {
          abort('418','メール送信に失敗しました。');
        }

      } catch (\Exception $e){
          abort('418','エラーが発生しました。');
      }

    }

    public function login(Request $request) {
        try {
            //①ログイン成功
            $user = \App\Tenant::where('email', $request->email)->where('password',$request->password)->first();
            if (isset($user)) {
                //②QRコードを作成→メール送信
                Mail::to($request->email)->send(new SendMail($user));
            } else {
                // カスタムエラーコード＝418
                abort('418','登録情報が見つかりませんでした。');
            }
            //③ 登録完了画面
            return redirect('/tenant/complete');
        } catch (\Exception $e) {
            abort('418','QRコードの再発行に失敗しました。');
        }
    }

}
