<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TenantPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required|max:255',
            'post_code' => 'required|max:7',
            'email' => 'string|required|email|max:100|confirmed',
            'password' => 'string|required|min:8|confirmed',
            'tenant_category_name' => 'string|in:
                "飲食店","劇場・映画館","会議室","パチンコ店","キャバレー・バー・パブ","ライブハウス","マージャン店",
                "商業施設(小売店)","博物館","体育館","展示場・集会所・ホール","ゲームセンター","ネットカフェ・個室ビデオ","カラオケボックス","ダンスホール","ドーム球場","場外馬券場","百貨店","商業施設(モール)","テーマパーク(遊園地)"',
            'judle_type' => 'string|in:"席数","面積"',
            'capacity' => 'string|in:
                "50席未満","50〜300席未満","300席以上"
                "1000m2以下","1000m2以上"',
        ];
    }

    public function messages()
    {
        return [
          'name.required' => '店舗名を入力してください。',
          'name.max' => '店舗名は255字以内でお願いします。',
          'post_code.required' => '郵便番号を入力してください。',
          'post_code.required' => '郵便番号の値が不正です。',
          'password.required' => 'パスワードを入力してください。',
          'password.min' => 'パスワードは8字以上でお願いします。',
          'password.confirmed' => '確認用パスワードと一致しません。',
          'tenant_category_name.in' => '営業形態の値が不正です。',
          'capacity.in' => '営業規模の値が不正です。',
        ];
    }

}
