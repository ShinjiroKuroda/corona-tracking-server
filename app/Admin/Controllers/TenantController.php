<?php

namespace App\Admin\Controllers;

use App\Tenant;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TenantController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Tenant';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Tenant());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('post_code', __('Post code'));
        $grid->column('location', __('Location'));
        $grid->column('email', __('Email'));
        $grid->column('phone_number', __('Phone number'));
        $grid->column('password', __('Password'));
        $grid->column('tenant_category_name', __('Tenant category name'));
        $grid->column('judle_type', __('Judle type'));
        $grid->column('capacity', __('Capacity'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Tenant::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('post_code', __('Post code'));
        $show->field('location', __('Location'));
        $show->field('email', __('Email'));
        $show->field('phone_number', __('Phone number'));
        $show->field('password', __('Password'));
        $show->field('tenant_category_name', __('Tenant category name'));
        $show->field('judle_type', __('Judle type'));
        $show->field('capacity', __('Capacity'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Tenant());

        $form->text('name', __('Name'));
        $form->text('post_code', __('Post code'));
        $form->text('location', __('Location'));
        $form->email('email', __('Email'));
        $form->text('phone_number', __('Phone number'));
        $form->password('password', __('Password'));
        $form->text('tenant_category_name', __('Tenant category name'));
        $form->text('judle_type', __('Judle type'));
        $form->text('capacity', __('Capacity'));

        return $form;
    }
}
