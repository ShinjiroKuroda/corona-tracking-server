<?php

namespace App\Admin\Controllers;

use App\TenantFootstamp;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TenantFootstampController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\TenantFootstamp';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new TenantFootstamp());

        $grid->column('id', __('Id'));
        $grid->column('user_id', __('User id'));
        $grid->column('tenant_id', __('Tenant id'));
        $grid->column('is_infected', __('Is infected'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(TenantFootstamp::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('user_id', __('User id'));
        $show->field('tenant_id', __('Tenant id'));
        $show->field('is_infected', __('Is infected'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new TenantFootstamp());

        $form->text('user_id', __('User id'));
        $form->number('tenant_id', __('Tenant id'));
        $form->switch('is_infected', __('Is infected'));

        return $form;
    }
}
