<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantFootstamp extends Model
{
    
    protected $fillable = ['user_id' , 'tenant_id' , 'is_infected'];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function tenant()
    {
        return $this->belongsTo('App\Tenant');
    }

}
