<?php

namespace App\Mail;
use App\Tenant;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendThank extends Mailable
{
    use Queueable, SerializesModels;

    public $tenant;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct(Tenant $tenant)
     {
         $this->tenant = $tenant;
     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.thank')->subject('ご協力頂きありがとうございます。');
    }
}
