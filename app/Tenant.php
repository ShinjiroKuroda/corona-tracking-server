<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{

    protected $fillable = ['name' , 'post_code' , 'location', 'email', 'phone_number', 'password', 'tenant_category_name', 'judle_type', 'capacity'];

    public function footstamps()
    {
        return $this->hasMany('App\TenantFootstamp');
    }

}
