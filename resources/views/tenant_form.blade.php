<!DOCTYPE html>

<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    <script src="{{ asset('js/tenant_form.js') }}"></script>
    <title>事業者フォーム</title>
</head>

<body>

    <div class="container">
      <div class="logo">
          <img src="{{ asset('images/osaka_logo.png') }}" alt="大阪府ロゴ" width="100" style="margin-top: 15px;">
      </div>
        <div class="mt-3">
            <p>大阪府追跡システムへの登録にご協力いただきありがとうございます。<br>
                入力フォームに注意事項をご入力いただき、注意事項を
                ご確認のうえ、登録ボタンをクリックしてください。
            </p>
        </div>

        <form action="/save/tenant" method="POST" class="mb-4" autocomplete="off">
            @csrf
            <div class="form-group">
                <label for="storename">○施設名</label>
                <input type="text" class="form-control mb-2" id="storename" name="name" placeholder="例）〇〇屋 ××店" required>
                <small class="text-muted ">※複数店舗登録希望の方は、各店舗ごとに登録してください。</small>
                @if ($errors->has('name'))
                    <small class="help-block text-danger">
                        ※{{ $errors->first('name') }}
                    </small>
                @endif
            </div>
            <label>○所在地</label>
            <div class="form-group row">
                <label for="zipcode" class="col-sm-0 ml-3 pt-1">郵便番号</label>
                <div class="col-sm-7 mb-3">
                    <input type="text" class="form-control" id="zipcode" name="post_code" maxlength="7" placeholder="例）1234567">
                    @if ($errors->has('post_code'))
                        <small class="help-block text-danger">
                            ※{{ $errors->first('post_code') }}
                        </small>
                    @endif
                </div>
                <div class="col-sm-auto">
                    <button type="button" id="search_btn" class="btn btn-secondary mb-3">　検索　</button>
                </div>
                <div class="ml-3 text-danger" id="zip_result"></div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="address_1" placeholder="郵便番号検索をご利用ください。" name="location_1" required>
                    @if ($errors->has('location_1'))
                        <small class="help-block text-danger">
                            ※{{ $errors->first('location_1') }}
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="address_2" placeholder="例）1-2-3-405" name="location_2" required>
                <small class="text-muted ">所在地が大阪府内にある施設のみ登録可能です。</small>
                @if ($errors->has('location_2'))
                    <small class="help-block text-danger">
                        ※{{ $errors->first('location_2') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
              <label for="tenantType">◯営業形態</label>
              <select id="tenantType" class="form-control" required name="tenant_category_name">
                <option value="未選択">選択...</option>
                <option value="飲食店">飲食店</option>
                <option value="劇場・映画館">劇場・映画館</option>
                <option value="会議室">会議室</option>
                <option value="パチンコ店">パチンコ店</option>
                <option value="キャバレー・バー・パブ">キャバレー・バー・パブ</option>
                <option value="ライブハウス">ライブハウス</option>
                <option value="マージャン店">マージャン店</option>

                <option value="商業施設">商業施設</option>
                <option value="博物館">博物館</option>
                <option value="体育館">体育館</option>
                <option value="展示場・集会所・ホール">展示場・集会所・ホール</option>
                <option value="ゲームセンター">ゲームセンター</option>
                <option value="ネットカフェ・個室ビデオ">ネットカフェ・個室ビデオ</option>
                <option value="カラオケボックス">カラオケボックス</option>
                <option value="ダンスホール">ダンスホール</option>
                <option value="ドーム球場">ドーム球場</option>
                <option value="場外馬券場">場外馬券場</option>
                <option value="百貨店">百貨店</option>
                <option value="商業施設(モール)">商業施設(モール)</option>
                <option value="テーマパーク・遊園地">テーマパーク・遊園地</option>
              </select>
              @if ($errors->has('tenant_category_name'))
                <small class="help-block text-danger">
                    ※{{ $errors->first('tenant_category_name') }}
                </small>
              @endif
            </div>

            <input type="hidden" id="judleType" name="judle_type" value="">

            <div class="form-group">
              <label for="capacity">◯営業規模</label>
              <select id="capacity" class="form-control" required name="capacity" disabled>
                <option>選択...</option>
              </select>
              @if ($errors->has('capacity'))
                  <small class="help-block text-danger">
                      ※{{ $errors->first('capacity') }}
                  </small>
              @endif
            </div>

            <div class="form-group">
                <label for="tel">○電話番号</label>
                <input type="tel" class="form-control mb-2" id="tel" placeholder="例）0669410351" required name="phone_number">
                @if ($errors->has('phone_number'))
                    <small class="help-block text-danger">
                        ※{{ $errors->first('phone_number') }}
                    </small>
                @endif
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">○メールアドレス</label>
              <input type="email" class="form-control mb-2" id="exampleInputEmail1 email" placeholder="例）abc@xxx.yy.jp" name="email">
              @if ($errors->has('email'))
                  <small class="help-block text-danger">
                      ※{{ $errors->first('email') }}
                  </small>
              @endif
              <small class="text-muted ">確認のため、もう一度入力してください。</small>
              <input type="email" class="form-control mt-2" id="exampleInputEmail2 email" placeholder="例）abc@xxx.yy.jp" required name="email_confirmation">
            </div>

            <div class="form-group">
              <label for="password">○パスワード</label>
              <input type="password" class="form-control mb-2" id="password" placeholder="例）半角英数字 8文字以上" name="password">
              @if ($errors->has('password'))
                  <small class="help-block text-danger">
                      ※{{ $errors->first('password') }}
                  </small>
              @endif
              <small class="text-muted ">確認のため、もう一度入力してください。</small>
              <input type="password" class="form-control mt-2" id="password_check" placeholder="例）半角英数字 8文字以上" required name="password_confirmation">
            </div>

            <div class="mt-4">
                <b>【注意喚起】</b>
                <ul>
                    <li class="mb-4">
                        大阪府個人情報条例にご了承の上、ご利用ください。
                        <div class="form-check">
                            <label class="form-check-label">
                                <input id="check" class="form-check-input" type="checkbox" value="" required>
                                同意します。
                            </label>
                        </div>
                    </li>
                    <li>
                        登録メールアドレスあて、QRコードをお送りいたします。
                    </li>
                    <li class="text-danger">
                        大阪府のメールは、ドメイン<br>
                        (@gbox.pref.osaka.lg.jp)から届きます。<br>
                        (ドメインにより受信制限されている方は解除をお願いします。)
                    </li>
                    <li>
                        いただいたメールアドレスは上記目的以外には使用せず、<br>
                        個人情報保護条例等に測り大阪府が適切に管理します。
                    </li>
                    <li>
                        マニュアルについては下記よりダウンロードください。<br>
                        <a href="#">http://www.pref.osaka.lg.jp/●●.html</a>
                    </li>
                </ul>
                <div class="text-center">
                    <button type="submit" id="submit" class="btn btn-primary mb-3"
                        data-disable-with="送信中...">　登録　</button>
                </div>
        </form>　
    </div>
    </div>
</body>

</html>
