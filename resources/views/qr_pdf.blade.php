


<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>

    h1, h2{
        margin: 0;
        padding: 0;
    }

    h1{
       text-align: center;
    }

    p {
        color: red;
    }

    small{
        margin-left: 200px;
    }


{
        list-style: none;
    }

    .text_normal{
        font-size: medium;
        font-weight: normal;
    }

    .img_main{
        position: relative;
        margin-top: 50px;
        margin-bottom: 150px;
    }

    .img_character img{
        z-index: -1;
        position: absolute;
        top: 75px;
        left: 0;
        right: 300px;
        bottom: 0;
        margin: auto;
    }

    .img_qrcode{
       margin-top: 20px;
       text-align: center;
    }

    .img_qrcode img{
        width: 200px;
        height: 200px;
    }

    .logo {
        text-align: right;
    }

    .logo img{
        width: 150px;
        height: 50px;
    }


    </style>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="{{ asset('images/osaka_logo.png') }}" alt="大阪府ロゴ">
        </div>
        <h2>大阪コロナ追跡システムご協力のお願い</h2>
                <h3>■大阪コロナシステムとは</h3>
                　本システムは、新型コロナウイルス感染症の<b><u>感染拡大を防ぐことを目的</u></b>として、お店等
                の利用の際にQRコードからメールアドレスを登録することより、<b><u>同じ日に同じ店等を利
                用された方が感染者になった場合、</u></b>あなたは登録された連絡先に<b><u>注意喚起のメールを送
                信</u></b>する仕組みになっています。
                <br>
                <div class="img_qrcode">
                  <img src="{{ asset('images/' . $qr_png_name) }}" alt="" width="200" height="200">
                </div>

                <h3>■登録の方法 <span class="text_normal">登録は3ステップです。</span></h3>
                <br>
                    1. <strong>スマートフォン等</strong>でQRコードを読み取る。<br>
                    2. 入力フォームに<b><u>メールアドレス</u></b>を入力する <br>
                    3. 登録メールが届く。
                <p>登録確認メールが届かない場合は登録できていない可能性があります。
                    お手数ですが再度ご登録をお願い致します。
                </p>
            <br>
                <h3>■ご注意下さい</h3>
                <br>
                    ◆　入力が必要な事項は<b><u>メールアドレスのみ</u></b>です。<br>
                    ◆　お店ごとに、来店時に<b><u>１日１回</u></b>だけご登録下さい。
        <br>
    </div>


</body>
</html>
