<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>事業者用のメール</title>
    <style>
        ul {
            padding-inline-start: 20px;
        }

        .pref{
            margin-right: 20px;
            text-align: right;
        }
    </style>
</head>

<body>
    <div class="logo">
        <img src="http://tm-apptime.sakura.ne.jp/public/images/osaka_logo.png" alt="大阪府ロゴ" width="100" style="margin-top: 15px;">
    </div>
    <p>大阪府コロナ追跡システムの登録にご協力いただきありがとうございます。</p>
    <ul>
        <li>
            本メールが事業者登録を完了したことをお知らせするメールです。
        </li>
        <li>
            下記のリンクからQRコードを記載した掲示板用の用紙をダウンロードしていただき、<br>
            印刷したうえ、掲示していただきますようお願いします。<br>
            <a href="#">http://www.pref.osaka.lg.jp/▲▲▲▲.html</a>
        </li>
        <br>
        <li>
            掲示方法などにつきましては、下記をご参照ください。<br>
            <a href="#">http://www.pref.osaka.lg.jp/●●●●.html</a>
        </li>
        <br>
        <li>
            本システムの登録に心当たりがない場合は、下記連絡先まで問い合わせください。<br>
        </li>
        <br>
        <li>
            マニュアルについては下記よりダウンロードください。<br>
            <a href="#">http://www.pref.osaka.lg.jp/●●●●.html</a>
        </li>
    </ul>
    <p>
        【連絡先】 <br>
        大阪コロナ追跡システム　コールセンター <br>
        06-6944-0000 <br>
    </p>
    <br>
    <p class="pref">大阪府</p>
</body>
</html>
