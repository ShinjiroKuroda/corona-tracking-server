<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>利用者用のメール</title>
    <style>
        ul {
            padding-inline-start: 20px;
            padding-left: 15px;
        }

        .alert {
            color: red;
            font-weight: bold;
        }

        .faq {
            padding: 0;
            list-style: none;
        }

        .pref {
            margin-right: 20px;
            text-align: right;
        }

    </style>
</head>

<body>
    <div class="logo">
        <img src="http://tm-apptime.sakura.ne.jp/public/images/osaka_logo.png" alt="大阪府ロゴ" width="100" style="margin-top: 15px;">
    </div>
    <p>大阪府コロナ追跡システムの登録にご協力いただきありがとうございます。</p>
    <ul>
        <li>
            本メールは、あなたが{{ $tenant->name }}を利用したことをお知らせするメールです。
            <p class="alert">
                ＜ご注意ください！＞<br>
                大阪コロナ追跡システムでは、今回ご登録いただいたメール <br>
                アドレス以外に、「氏名」、「住所」、「電話番号」をはじめ一切 <br>
                の個人情報を収集する事はございません。
            </p>
        </li>
        <br>
        <li>
           登録日より○日以内に、施設内で感染者が発生した場<br>
           合、大阪府より注意喚起メールをお送りします。
        </li>
        <li>
            登録に心当たりがない場合は、下記連絡先まで問い合わせください。<br>
        </li>
        <br>
    </ul>

    <ul class="faq">
        <li>利用者にあたってご不明な点は、こちらをご参照ください。</li>
        <br>
        <li>【利用者FAQ】</li>
    </ul>

    <p>
        【連絡先】 <br>
        ▲▲▲▲<br>
        06-6944-▲▲▲▲ <br>
    </p>
    <br>
    <p class="pref">大阪府</p>
</body>
</html>
