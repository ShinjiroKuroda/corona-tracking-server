
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    <title>利用者登録フォーム</title>
    <style media="screen">
      ul {
        padding-left: 15px;
      }
    </style>
</head>

<body>
  <div class="logo">
      <img src="/public/images/osaka_logo.png" alt="大阪府ロゴ" width="100" style="margin-top: 15px;">
  </div>
    <div class="container">
        <div class="mt-2">
            <p>大阪府追跡システムへの登録にご協力いただきありがとうございます。
                テキストボックスにメールアドレスを入力いただき、注意事項を
                ご確認のうえ、登録ボタンをクリックしてください。
            </p>
        </div>

        <form action="/regist/user" method="POST" class="mb-4">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">○メールアドレス</label>
                <input type="email" class="form-control mb-2" id="exampleInputEmail1　email"
                    placeholder="例) abc@xxx.yy.jp" required name="email">
                <small class="text-muted ">確認のため、もう一度入力してください。</small>
                <input type="hidden" name="secret_code" value="{{$id}}">
                <input type="email" class="form-control mt-2" id="exampleInputEmail1 email" placeholder="例) abc@xxx.yy.jp">
                <p id="msg-email"></p>
            </div>
            <div>
                <b>【注意喚起】</b>
                <ul>
                    <li>
                        登録メールアドレス宛に確認メールをお送りします。
                    </li>
                    <li>
                        あなたが登録した日と同じ日に、同じ施設を利用された
                        方が陽性者と判断した場合、大阪府より注意喚起メール
                        をお送りします。
                    </li>
                    <li class="text-danger">
                        大阪府のメールは、ドメイン
                        (@gbox.pref.osaka.lg.jp)から届きます。
                        (ドメインにより受信制限されている方は解除をお願いします。)
                    </li>
                    <li>
                        いただいたメールアドレスは上記目的以外には使用せず、
                        個人情報保護条例等に測り大阪府が適切に管理します。
                    </li>
                    <br>
                    <li>
                        マニュアルについては下記よりダウンロードください。<br>
                        <a href="#">http://www.pref.osaka.lg.jp/●●.html</a>
                    </li>
                </ul>
                <div class="text-center">
                    <button type="submit" id="submit" class="btn btn-primary mb-3"
                        data-disable-with="送信中...">　登録　</button>
                </div>

                <div class="text-center">
                    <b class="text-danger">
                        <u>
                            大阪コロナ追跡システムでは、今回ご登録いただくメール
                            アドレス以外に、「氏名」「住所」「電話番号」をはじめ一切
                            の個人情報を収集することはございません。
                        </u>
                    </b>
                </div>
            </div>
        </form>
    </div>
</body>

</html>
