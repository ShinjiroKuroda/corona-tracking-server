<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 Forbidden</title>
</head>

<body>
    <div class="logo">
        <img src="http://tm-apptime.sakura.ne.jp/public/images/osaka_logo.png" alt="大阪府ロゴ" width="100" style="margin-top: 15px;">
    </div>
    <div class="jumbotron">
        <hr class="my-4">
        <p>{{ $exception->getMessage() }}</p>
        <p class="lead">
        <a class="btn btn-primary" href="javascript:history.back()" role="button">前のページへ戻る</a>
        </p>
    </div>
</body>
</html>
