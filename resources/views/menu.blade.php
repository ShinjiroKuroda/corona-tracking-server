<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>サインインページ</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <link rel="stylesheet" href="http://tm-apptime.sakura.ne.jp/vendor/laravel-admin/number-input/bootstrap-number-input.js">
</head>

<style media="screen">

  html,
  body {
    height: 100%;
  }

  body {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    padding-top: 40px;
    padding-bottom: 40px;
  }

  .form-signin {
    width: 100%;
    max-width: 330px;
    padding: 15px;
    margin: auto;
  }
  .form-signin .checkbox {
    font-weight: 400;
  }

  .form-signin .form-control {
    position: relative;
    box-sizing: border-box;
    height: auto;
    padding: 10px;
    font-size: 16px;
  }

  .form-signin .form-control:focus {
    z-index: 2;
  }

  .form-signin input[type="email"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
  }
  
  .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }

</style>

<body class="text-center">
  <form class="form-signin" action="/login/tenant" method="POST">
    <img class="mb-4" src="{{ asset('images/mozu_yan.jpeg') }}" alt="" width="150" height="150">
    <h1 class="h3 mt-3 font-weight-bold text-secondary">コロナ対策システム</h1>
    <h4 class="mb-3 font-weight-bold text-secondary">事業者様画面</h4>

    @csrf

    <label for="inputEmail" class="sr-only">メールアドレス</label>
    <input type="email" id="inputEmail" class="form-control" required name="email" placeholder="メールアドレスを入力">

    <label for="inputPassword" class="sr-only">パスワード</label>
    <input type="password" id="inputPassword" class="form-control" required name="password" placeholder="パスワードを入力">

    <button class="btn btn-lg btn-primary btn-block" type="submit">QRコードを再発行する</button>
    <a class="btn btn-lg btn-success btn-block" href="{{ url('/tenant/form') }}">新規登録する</a>

  </form>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd" crossorigin="anonymous"></script>
</body>

</html>
