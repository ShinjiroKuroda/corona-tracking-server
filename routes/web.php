<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tenant', function () {
  return view('menu');
});

Route::get('/tenant/form', function () {
  return view('tenant_form');
});

Route::get('/tenant/complete', function () {
  return view('complete_tenant');
});

Route::post('save/tenant', 'TenantController@regist');

Route::post('login/tenant', 'TenantController@login');

Route::get('/login/{id}', 'UserController@login');

Route::post('regist/user', 'UserController@regist');

Route::get('/footprint/complete', function () {
  return view('footprint_complete');
});
